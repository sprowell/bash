# BASH/ZSH Extensions

## mk.sh

A little file that provides the stupid-simple `mk` build tool.  Read about it [here](mk.md).

The [license](LICENSE) exists to protect your ability to steal any of this, and to protect me from the consequences of your bad decisions.

### Installation / Uninstallation

To install or uninstall, just run the script.

```
$ bash mk.sh
INSTALL/UNINSTALL mk UTILITY
============================

Version: 3.0.0

Running this script will modify your .bashrc and/or .zshrc files to source
this script (install) or stop sourcing it (uninstall).

Do you want to continue (enter u to uninstall)? (y/N/u)
```

Respond with `y` to install, `u` to uninstall, or anything else to exit without making any changes.

### Manual Installation

Add the following to the end of the `~/.bashrc` to use the `mk` utility, pointing to the correct location of this script, of course.

```
source mk.sh
```

## dis.sh

A little file that defines some aliases for disassembly.  If you don't want to disassemble code, ignore these.

- `dis` will disassemble a file (using objdump) with the Intel syntax
- `vdis` will disassemble showing jumps
- `dis` will disassemble a file and output through a pager
- `vdis` will disassemble showing jumps and output through a pager

These try to use the pager `bat` if available.  If not, then they fallback to `most`, `less`, and finally `more`, using the first available.  If you want to specify a *particular* pager, set `__PAGER__` to the command to use before sourcing the script.

You can also set `__OBJDUMP_OPTS__` to add arbitrary options to the objdump command line.

By default Intel syntax is used for the X86 family.  To use AT&T syntax, set `__OBJDUMP_DISASSEMBLY_FLAVOR__` to the empty string prior to sourcing the script.

To prevent using color, set `__OBJDUMP_DISASSEMBLY_COLOR__` to the empty string before sourcing the script.

### Manual Installation

Add the following to the end of the `~/.bashrc` to use these aliases, pointing to the correct location of this script, of course.

```
source mk.sh
```

## Versioning

This project uses [semantic versioning](https://semver.org/).

## Contributing

Feel free to contribute.  All contributions will be subsumed under the [license](LICENSE).

Does this script work for you?  Check if your platform is covered for the current version in `TESTED.md` and, if not, consider running the script `add_test_record.sh` and then submitting the updated `TESTED.md` file to record success!
