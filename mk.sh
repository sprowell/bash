#!/bin/bash

# mk.sh - simple build script
# Source this from the end of your .bashrc or .zshrc
#
# Copyright (c) 2023 by Stacy Prowell (sprowell@gmail.com).
#
# license: 0BSD
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

# Get the path to this script.
mkpath="$( cd -- "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P || exit)/$(basename "${BASH_SOURCE[0]}")"

# Version number.
_MK_VERSION="3.0.0"

# If we are being run directly, install or uninstall.  Otherwise if we are being
# sourced, skip this.
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
    cat <<ENDNOTE1
INSTALL/UNINSTALL mk UTILITY
============================

Version: ${_MK_VERSION}

Running this script will modify your .bashrc and/or .zshrc files to source
this script (install) or stop sourcing it (uninstall).

ENDNOTE1
    uninstall=
    read -r -p "Do you want to continue (enter u to uninstall)? (y/N/u) " answer
    case $answer in
        [yY]*)
            ;;
        [uU]*)
            echo "Uninstalling."
            uninstall=1
            ;;
        *)
            echo "Stopping at user request."
            exit 0
            ;;
    esac
    for file in "$HOME/.bashrc" "$HOME/.zshrc" ; do
        if [ -e "$file" ] ; then
            echo -n "Updating $file... "
            grep -v "# SJP:MK" "$file" > "$file.new"
            cp -f "$file" "$file.save"
            [ -z $uninstall ] && echo "source \"${mkpath}\" # SJP:MK" >> "$file.new"
            mv -f "$file.new" "$file"
            echo "Done"
        fi
    done
    if [ -z $uninstall ] ; then
        cat <<ENDNOTE2

Installation complete.  If you need to remove this or disable it, run this
script again.
ENDNOTE2
    else
        cat <<ENDNOTE3

Uninstall complete.  If you need to re-install this script, run this script
again.
ENDNOTE3
    fi
    exit 0
fi

# Write a debugging message.
function _mk_debug () {
    printf "\e[35m[debug] $(date +'%F %T'):\e[0m %s\n" "$@"
}

# Write an error message.
function _mk_error () {
    printf "\e[31m\e[1m[ERROR]\e[0m %s\n" "$@"
}

# Build a program (usually assembly) using the first line as the build
# instructions.  Run this with -h to find out more about how to use it.
function mk () {
    local assembler
    local linker
    local dynlinker
    local compiler
    local files
    [ -z "$mkASSEMBLER"  ] && assembler="$mkASSEMBLER"
    [ -z "$mkLINKER"  ] && linker="$mkLINKER"
    [ -z "$mkDYN_LINKER" ] && dynlinker="$mkDYN_LINKER"
    [ -z "$mkCC_COMPILER"  ] && compiler="$mkCC_COMPILER"
    # Supported platforms: 64-bit Intel and 64-bit ARM.
    # TODO: Get this working on the new Apple M1 chip.
    case "$(uname -m)/$(uname -s)" in
        armv7l*)
            [ -z "$assembler" ] && assembler=as
            [ -z "$linker"    ] && linker=ld
            [ -z "$dynlinker" ] && dynlinker="ld -dynamic-linker /lib/ld-linux-armhf.so.3"
            [ -z "$compiler"  ] && compiler=gcc
            ;;
        aarch64*)
            [ -z "$assembler" ] && assembler=as
            [ -z "$linker"    ] && linker=ld
            [ -z "$dynlinker" ] && dynlinker="ld -dynamic-linker /lib/ld-linux-aarch64.so.1"
            [ -z "$compiler"  ] && compiler=gcc
            ;;
        x86_64/Linux*)
            [ -z "$assembler" ] && assembler="nasm -f elf64"
            [ -z "$linker"    ] && linker=ld
            [ -z "$dynlinker" ] && dynlinker="ld -dynamic-linker /lib64/ld-linux-x86-64.so.2"
            [ -z "$compiler"  ] && compiler=gcc
            ;;
        x86_64/Darwin*)
            [ -z "$assembler" ] && assembler="nasm -fmacho64"
            [ -z "$linker"    ] && linker="ld -static"
            [ -z "$dynlinker" ] && dynlinker="ld -dynamic"
            [ -z "$compiler"  ] && compiler=gcc
            ;;
        *)
            _mk_error "Unsupported platform/OS: $(uname -m)/$(uname -s)"
            return 1
            ;;
    esac
    local debug
    local file
    files=()
    for opt in "${@}" ; do
        case "$opt" in
            -h*|--help)
                cat <<ENDHELP
usage: mk [options] [source files...]

Options:
  -d | --debug ....... Print debugging information and exit.
  -h | --help ........ Print this help information.
  --version .......... Print the current version and exit.

If the first line of a file contains the character sequence "mk:" then
interpret everything after on the line as build instructions and execute those
instructions.

Example 1:
mk: nasm -felf64 hello.asm && ld -o hello hello.o

In the above example we can use some variable substitutions.  For instance,
\$mkFILE expands to the input file, and \$mkBASE expands to the filename without
any extension.  We can rewrite the above as follows.

Example 2:
mk: nasm -felf64 \$mkFILE && ld -o \$mkBASE \$mkBASE.o

There are some additional variables that encode the above, with a few extra
options.

  - \$mkAS ....... expands to the "usual" assembly command
  - \$mkLD ....... expands to the "usual" link command with the linker
  - \$mkLDD ...... expands to the "usual" dynamic link command with the linker
  - \$mkCC ....... expands to the "usual" C compile command
  - \$mkCL ....... expands to the "usual" static link command via the compiler
  - \$mkCLD ...... expands to the "usual" dynamic link command via the compiler

Options can be passed to each of the commands using the environment variables.
These are treated as the empty string if not set.

  - \$mkASOPTS ..... options to pass to the assembler
  - \$mkLDOPTS ..... options to pass to the linker
  - \$mkCCOPTS ..... options to pass to the compiler
  - \$mkCLOPTS ..... options to pass to the compiler (when called to link)

We can rewrite the prior example as follows.

Example 3:
mk: \$mkAS && \$mkLD

To specify the options, set them prior to the command.  For example, the
following will produce a listing file (for AS) and link statically (for
LD).

mkASOPTS=-lout.lst mkLDOPTS=-static mk hello.asm

Note that mk can be called recursively, so the following will work.

Example 4:
mk: mk print.asm && mk read.asm && \$mkLD -o final print.o read.o

ENDHELP
                return 0
                ;;
            -d*|--debug)
                _mk_debug "Debugging enabled."
                debug="echo"
                ;;
            --version)
                echo "version: $_MK_VERSION"
                return 0
                ;;
            -*)
                _mk_error "Unrecognized command line switch $opt"
                return 1
                ;;
            *)
                files=("${files[@]}" "$opt")
                ;;
        esac
        shift
    done
    [ -z "${files[0]}" ] && {
        _mk_error "Missing file name."
        return 1
    }
    [ -z "$mkASOPTS" ] && export mkASOPTS=""
    [ -z "$mkLDOPTS" ] && export mkLDOPTS=""
    [ -z "$mkCCOPTS" ] && export mkCCOPTS=""
    [ -z "$mkCLOPTS" ] && export mkCLOPTS=""
    for file in "${files[@]}" ; do
    (
        export \
            mkFILE="$file" \
            mkBASE="${file%.*}" \
             mkEXT="${file##*.}"
        export \
              mkAS="$assembler $mkASOPTS    -o \"$mkBASE.o\" \"$mkFILE\"   " \
              mkLD="$linker    $mkLDOPTS    -o \"$mkBASE\"   \"$mkBASE.o\" " \
             mkLDD="$dynlinker $mkLDOPTS    -o \"$mkBASE\"   \"$mkBASE.o\" " \
              mkCC="$compiler  $mkCCOPTS -c -o \"$mkBASE.o\" \"$mkFILE\"   " \
              mkCL="$compiler  $mkCLOPTS -static -o \"$mkBASE\" \"$mkBASE.o\" " \
             mkCLD="$compiler  $mkCLOPTS    -o \"$mkBASE\"   \"$mkBASE.o\" " 
        case "$mkEXT" in
            asm|nasm|s)
                export  mk="$mkAS && $mkLD"
                export mkD="$mkAS && $mkLDD"
                ;;
            *)
                export  mk="$mkCC && $mkCL"
                export mkD="$mkCC && $mkCLD"
                ;;
        esac
        # Replaced use of shopt -s lastpipe here to support zsh and MacOS.
        read -r mkCMD < <(head -1 "$mkFILE" | sed -ne "s/^.*mk:\(.*\)$/\1/p" | tr -d '\r' | envsubst)
        [ -z "$mkCMD" ] && {
            _mk_error "Did not find mk: line; no build instructions."
            return 2
        }
        [ -z "$debug" ] || {
            _mk_debug "---- For file: $file ----"
            _mk_debug ""
            _mk_debug "From the file name:"
            _mk_debug "mkFILE        = $mkFILE"
            _mk_debug "mkBASE        = $mkBASE"
            _mk_debug "mkEXT         = $mkEXT"
            _mk_debug ""
            _mk_debug "Platform commands:"
            _mk_debug "mkASSEMBLER   = $assembler"
            _mk_debug "mkLINKER      = $linker"
            _mk_debug "mkDYN_LINKER  = $dynlinker"
            _mk_debug "mkCC_COMPILER = $compiler"
            _mk_debug ""
            _mk_debug "Command options:"
            _mk_debug "mkASOPTS      = $mkASOPTS"
            _mk_debug "mkLDOPTS      = $mkLDOPTS"
            _mk_debug "mkCCOPTS      = $mkCCOPTS"
            _mk_debug "mkCLOPTS      = $mkCLOPTS"
            _mk_debug ""
            _mk_debug "Common elements:"
            _mk_debug "mkAS          = $mkAS"
            _mk_debug "mkLD          = $mkLD"
            _mk_debug "mkLDD         = $mkLDD"
            _mk_debug "mkCC          = $mkCC"
            _mk_debug "mkCL          = $mkCL"
            _mk_debug "mkCLD         = $mkCLD"
            _mk_debug ""
            _mk_debug "All-in-one commands:"
            _mk_debug "mk            = $mk"
            _mk_debug "mkD           = $mkD"
            _mk_debug ""
            _mk_debug "Command for file: $file:"
            _mk_debug "mkCMD         = $mkCMD"
            return 0
        }
        echo "$mkCMD"
        eval "$mkCMD"
    )
    done
}
