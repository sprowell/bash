# A Quick Guide to mk

The `mk` command was created for CSC 6580 Advanced Reverse Engineering at Tennessee Tech.  It is a very, very simple build tool.

`mk` is a BASH or ZSH function that reads the first line of a file and looks for the `mk:` marker and then executes the command line it finds after it.  If the `mk:` line is not there, `mk` complains.

```text
$ mk fido.asm
[ERROR] Did not find mk: line; no build instructions.
```

## Motivation

When writing assembly sometimes a specific instruction is needed to assemble and link a file.  I started documenting this at the top of the code so students could build the examples in the slides easily, and then decided it would be pretty neat if we could just execute that top line automagically.  So I wrote `mk` to do that.

Why not just make a `Makefile`?  Well, the `Makefile` and the code won't fit on a slide together, but I can spare a single line at the top of the code.  Plus then I would need a make file for each file, or I would need to keep updating the `Makefile` when he code changes, etc.  Trust me.  This is simpler for this particular use case.

A nice thing about this is that you can create a file, say `mul1.asm` with a build line in it.  Then you can copy it to a new file, like `mul2.asm` and not have to change anything to get that to build.  This is really helpful for creating a series of examples for a class.

## Basic Use

`mk` looks at the first line of your source file for the characters `mk:`.  These can occur anywhere in the line.  `mk` then reads everything after `mk:` on the line and tries to interpret it as a build command.

~~~nasm
; This is a NASM comment. mk: nasm -felf64 myfile.asm -o myfile.o && ld -o myfile myfile.o
~~~

The above results in `mk` getting the build instruction `nasm -felf64 myfile.asm -o myfile.o && ld -o myfile myfile.o`.  Of course, what if the filename changes?  Well, we can replace it with a variable.

The variable `$mkFILE` is the current file name, and `$mkBASE` is the file's base name without the extension.  So we can replace the above line with the following.

~~~nasm
; mk: nasm -felf64 $mkFILE -o $mkBASE.o && ld -o $mkBASE $mkBASE.o
~~~

It's no shorter, but it does work if we rename the file.  These commands, the `nasm` and `ld` commands, are so common we might just as well have a variable that expands into each.  And we do: `$mkAS` and `$mkLD`.

~~~nasm
; mk: $mkAS && $mkLD
~~~

## Substitutions

`mk` performs variable substitution, which is how `$mkAS` and `$mkLD` work.  For example, using `mk` on the following file will list the contents of your home folder.

```text
mk: ls $HOME
```

You can pass variables to `mk` by defining them in the environment and exporting them.  For example, the following defines `mkLDOPTS`.

```bash
$ export mkLDOPTS="--print-map"
```

Alternately you can define the variable just for one invocation of `mk` by giving the variable setting just prior to invocation.

```bash
$ mkLDOPTS="--print-map"  mk hello.asm
```

## Variables that Expand to Commands

The following variables are defined by `mk` when it runs.

|Variable     |Use     |
|-------------|--------|
|`$mkFILE`    |The target file name |
|`$mkBASE`    |The target file name without any extension |
|`$mkEXT`     |The target file name extension |
|`$mkAS`      |The platform command to assemble a file. |
|`$mkLD`      |The platform command to link a file with the linker |
|`$mkLDD`     |The platform command to dynamically link a file with the linker |
|`$mkCC`      |The platform command to compile a file with the C compiler, but not link |
|`$mkCL`      |The platform command to link a file through the C compiler |
|`$mkCLD`     |The platform command to link a file through the C compiler dynamically |
|`$mk`        |Assemble or compile statically based on extension |
|`$mkD`       |Assemble or compile dynamically based on extension |

An important note is that a leading path in the filename is *preserved* in both `$mkFILE` and `$mkBASE`.  This only matters if you run `mk` from a different folder from the target.

## Variables that Expand to Configuration Options

You can add configuration options to the various commands by setting environment variables in the shell prior to invoking `mk`.

|Variable   |Use     |
|-----------|--------|
|`$mkASOPTS`  |Options to pass to the assembler command |
|`$mkLDOPTS`  |Options to pass to the linker command |
|`$mkCCOPTS`  |Options to pass to the compiler |
|`$mkCLOPTS`  |Options to pass to the compiler when linking |

## Platforms

`mk` expects to use `nasm` on X86 architectures, and to use GNU `as` on ARM.  The linker is expected to be `ld` and the compiler `gcc`.

You can override these choices by setting an environment variable prior to invoking.

|Variable   |Use     |
|-----------|--------|
|`$mkASSEMBLER`   |Specify the assembler command |
|`$mkLINKER`      |Specify the linker command |
|`$mkDYN_LINKER`  |Specify the dynamic linker command |
|`$mkCC_COMPILER` |Specify the compiler |

## Debugging

You can see what `mk` thinks of your build line by running it with the `--debug` flag.  This displays the various choices that have been made, along with the variable settings and the computed build line as `mkCMD` at the bottom.  The build line is *not* executed.

```text
$ mk --debug test.asm
[debug] 2024-01-06 14:51:39: Debugging enabled.
[debug] 2024-01-06 14:51:39: ---- For file: test.asm ----
[debug] 2024-01-06 14:51:39: 
[debug] 2024-01-06 14:51:39: From the file name:
[debug] 2024-01-06 14:51:39: mkFILE        = test.asm
[debug] 2024-01-06 14:51:39: mkBASE        = test
[debug] 2024-01-06 14:51:39: mkEXT         = asm
[debug] 2024-01-06 14:51:39: 
[debug] 2024-01-06 14:51:39: Platform commands:
[debug] 2024-01-06 14:51:39: mkASSEMBLER   = nasm -f elf64
[debug] 2024-01-06 14:51:39: mkLINKER      = ld
[debug] 2024-01-06 14:51:39: mkDYN_LINKER  = ld -dynamic-linker /lib64/ld-linux-x86-64.so.2
[debug] 2024-01-06 14:51:39: mkCC_COMPILER = gcc
[debug] 2024-01-06 14:51:39: 
[debug] 2024-01-06 14:51:39: Command options:
[debug] 2024-01-06 14:51:39: mkASOPTS      = 
[debug] 2024-01-06 14:51:39: mkLDOPTS      = 
[debug] 2024-01-06 14:51:39: mkCCOPTS      = 
[debug] 2024-01-06 14:51:39: mkCLOPTS      = 
[debug] 2024-01-06 14:51:39: 
[debug] 2024-01-06 14:51:39: Common elements:
[debug] 2024-01-06 14:51:39: mkAS          = nasm -f elf64     -o "test.o" "test.asm"   
[debug] 2024-01-06 14:51:39: mkLD          = ld        -o "test"   "test.o" 
[debug] 2024-01-06 14:51:39: mkLDD         = ld -dynamic-linker /lib64/ld-linux-x86-64.so.2     -o "test"   "test.o" 
[debug] 2024-01-06 14:51:39: mkCC          = gcc   -c -o "test.o" "test.asm"   
[debug] 2024-01-06 14:51:39: mkCL          = gcc   -static -o "test" "test.o" 
[debug] 2024-01-06 14:51:39: mkCLD         = gcc      -o "test"   "test.o" 
[debug] 2024-01-06 14:51:39: 
[debug] 2024-01-06 14:51:39: All-in-one commands:
[debug] 2024-01-06 14:51:39: mk            = nasm -f elf64     -o "test.o" "test.asm"    && ld        -o "test"   "test.o" 
[debug] 2024-01-06 14:51:39: mkD           = nasm -f elf64     -o "test.o" "test.asm"    && ld -dynamic-linker /lib64/ld-linux-x86-64.so.2     -o "test"   "test.o" 
[debug] 2024-01-06 14:51:39: 
[debug] 2024-01-06 14:51:39: Command for file: test.asm:
[debug] 2024-01-06 14:51:39: mkCMD         = nasm -f elf64     -o "test.o" "test.asm"    && ld        -o "test"   "test.o"
```

The last line is the one to look at; this is the command that `mk` will run to build your file (or do whatever else you want).  It is not executed when debugging.

## Common Use Cases

These all use NASM syntax for the comment.  If you are using GNU as or another assembler, correct the comment notation.

**Assemble but do not link.**

~~~nasm
; mk: $mkAS
~~~

**Build and link a program without library references.**

~~~nasm
; mk: $mkAS && $mkLD
~~~

The following should also work, depending on the file extension.

~~~nasm
; mk: $mk
~~~

**Build and statically link a program with the C runtime.**

~~~nasm
; mk: $mkAS && $mkCL -static
~~~

**Build and dynamically link a program.**

~~~nasm
; mk: $mkAS && $mkLDD
~~~

The following should also work, depending on the file extension.

~~~nasm
; mk: $mkD
~~~

**Build a C program.**

~~~c
// mk: $mkCC && $mkCL
~~~

If your C compiler does not support the nice single-line comment above, try the following.

~~~c
/* mk: $mkCC && $mkCL #*/
~~~

The `#` tells the shell the rest of the line is a comment.  Or you can just move `*/` to the next line.

The following should also work, depending on the file extension.

~~~c
// mk: $mk
~~~

**Build and dynamically link a program with the C runtime.**

~~~nasm
; mk: $mkAS && $mkCLD
~~~

**Create a "make all" or "make clean" target**

~~~text
mk: mk first.asm && mk second.asm && mk third.asm && mk fourth.asm && mk final.asm
~~~

You can use the `mk` function in your build line.  The above could be in a file named simply `all`.  Then `mk all` would trigger the complete build.

Likewise you can create a `mk clean` target with a file like the following in a file named `clean`.

~~~text
mk: rm -f *.o final
~~~

## The Build Line

The build instruction is just a line for the shell to execute.  You can see this by putting the following in the file `list.txt`.

~~~text
mk: ls $HOME
~~~

Running `mk list.txt` will give you a listing of the home directory.

This line is executed by the shell, and you can use all the various shell tricks.  Read up on the BASH or ZSH shell to see what you can do.  The following are some simple examples that should work in either shell.

**Perform a series of actions**

~~~nasm
; mk: ls $HOME ; ls .
~~~

To execute several actions, separate them with a semicolon `;`.  The actions all execute, whether or not any fail.  The above will list the user's home area, then list the current working directory.

**Perform a series of actions if each action succeeds**

~~~nasm
; mk: $mkAS && $mkLD && echo "Success!"
~~~

To execute several actions, stopping at the first one that fails, separate them with `&&`.  The above first tries to assemble the file and, if that is successful, then tries to link it.  If both work then it prints `Success!`.

**Perform an action if a prior action fails**

~~~nasm
; mk: $mkAS || echo "Could not assemble!"
~~~

The above line first tries to assemble the file and, if that *fails*, then prints `Could not assemble!`.

**If-then-else**

~~~nasm
; mk: if $mkAS ; then $mkLD ; else echo "Could not assemble!" ; fi
; mk: $mkAS || echo "Could not assemble!" && $mkLD
~~~

Both these essentially do the same thing, though the first is more reliable.  First the file is assembled.  If this works, it is then linked.  If it does not work, then `Could not assemble!` is printed.

**Check whether a file exists**

~~~nasm
; mk: [ -f pre.o ] || echo "Need pre.o" && $mkCC && $mkCL pre.o
~~~

Here we check for the file `pre.o` and if we don't find it, we write `Need pre.o`.  Otherwise we compile the current file and link it with `pre.o`.
