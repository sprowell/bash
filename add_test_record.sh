#!/bin/bash

# Just to be sure we have the version number...
source "./mk.sh"

echo "Recording test success for Version: $_MK_VERSION!"
{
    echo ""
    echo "## Test Successful for Version: $_MK_VERSION"
    echo "|Date              |$(date -u)    |"
    echo "|Kernel            |$(uname -srv) |"
    echo "|Hardware          |$(uname -m)   |"
    echo "|Operating System  |$(uname -o)   |"
    echo "|Processor         |$(uname -o)   |"
    [ -z "$BASH_VERSION" ] || echo "|bash version      |$BASH_VERSION |"
    [ -z "$ZSH_VERSION" ]  || echo "|zsh version       |$ZSH_VERSION  |"
}>>TESTED.md
