# Test Success Record

Record a successful test of the `mk` script here by running `add_test_record.sh` on your platform.

## Test Successful for Version: 2.0.0
|Date              |Sat Sep  9 11:10:48 PM UTC 2023    |
|Kernel            |Linux 6.2.0-32-generic #32-Ubuntu SMP PREEMPT_DYNAMIC Mon Aug 14 10:03:50 UTC 2023 |
|Hardware          |x86_64   |
|Operating System  |GNU/Linux   |
|Processor         |GNU/Linux   |
|zsh version       |5.9  |

## Test Successful for Version: 2.0.0
|Date              |Sat Sep  9 11:11:14 PM UTC 2023    |
|Kernel            |Linux 6.2.0-32-generic #32-Ubuntu SMP PREEMPT_DYNAMIC Mon Aug 14 10:03:50 UTC 2023 |
|Hardware          |x86_64   |
|Operating System  |GNU/Linux   |
|Processor         |GNU/Linux   |
|bash version      |5.2.15(1)-release |

## Test Successful for Version: 2.1.0
|Date              |Thu Oct 19 04:01:40 AM UTC 2023    |
|Kernel            |Linux 6.2.0-34-generic #34-Ubuntu SMP PREEMPT_DYNAMIC Mon Sep  4 13:06:55 UTC 2023 |
|Hardware          |x86_64   |
|Operating System  |GNU/Linux   |
|Processor         |GNU/Linux   |
|bash version      |5.2.15(1)-release |
