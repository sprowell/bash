# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Release information follows.  The following sections can be present.

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

----

## Unreleased

## 3.0.0

### Added

- Added `$mk` and `$mkD` all-in-one commands based on file extension.
- Added `$mkEXT` variable with file extension, so `$mkBASE` + `$mkEXT` = `$mkFILE`

### Changed

- Changed `$mkDYN` to `$mkLDD`.
- Changed debug to show more information.

### Removed

- Removed deprecated commands.

## 2.1.0

### Added

- Added installer / uninstaller to both mk.sh and dis.sh.

### Changed

- Changed to use printf and avoid escape characters in the file.
- General (slight) cleanup of bash syntax.

## 2.0.0

### Added

- The `mk` function now takes multiple arguments and tries to build each one, so it is possible to use `mk *.asm`, for instance
- Added a test log `TESTED.md` and a script to add entries `add_test_record.sh`
- Added a `CHANGELOG.md`

### Changed

- Changed the file name to `mk.sh` and split out the other aliases into their own file `dis.sh`

### Deprecated

- Made older `$mkGCC` and `$mkNASM` commands deprecated.

## 1.1.0

### Changed

- Added `$mkCC` to compile and `$mkCL` to link through the compiler, replacing `$mkGCC`
