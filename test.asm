; mk: $mk

global _start
section .text
_start:
    mov eax, 1
    mov edi, 1
    mov rsi, string
    mov edx, string.len
    syscall
    xor edi, edi
    mov eax, 60
    syscall
    hlt
section .data
string:
    db "Hello world!", 10, 0
.len equ $-string

