#!/bin/bash

# Additional aliases.
# Source this from the end of your .bashrc or .zshrc
#
# Copyright (c) 2023 by Stacy Prowell (sprowell@gmail.com).
#
# license: 0BSD
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.


# Get the path to this script.
mkpath="$( cd -- "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P || exit)/$(basename "${BASH_SOURCE[0]}")"

# Version number.
_MK_VERSION="2.1.0"

# If we are being run directly, install or uninstall.  Otherwise if we are being
# sourced, skip this.
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
    cat <<ENDNOTE1
INSTALL/UNINSTALL dis UTILITY
=============================

Version: ${_MK_VERSION}

Running this script will modify your .bashrc and/or .zshrc files to source
this script (install) or stop sourcing it (uninstall).

ENDNOTE1
    uninstall=
    read -r -p "Do you want to continue (enter u to uninstall)? (y/N/u) " answer
    case $answer in
        [yY]*)
            ;;
        [uU]*)
            echo "Uninstalling."
            uninstall=1
            ;;
        *)
            echo "Stopping at user request."
            exit 0
            ;;
    esac
    for file in "$HOME/.bashrc" "$HOME/.zshrc" ; do
        if [ -e "$file" ] ; then
            echo -n "Updating $file... "
            grep -v "# SJP:DIS" "$file" > "$file.new"
            cp -f "$file" "$file.save"
            [ -z $uninstall ] && echo "source \"${mkpath}\" # SJP:DIS" >> "$file.new"
            mv -f "$file.new" "$file"
            echo "Done"
        fi
    done
    if [ -z $uninstall ] ; then
        cat <<ENDNOTE2

Installation complete.  If you need to remove this or disable it, run this
script again.
ENDNOTE2
    else
        cat <<ENDNOTE3

Uninstall complete.  If you need to re-install this script, run this script
again.
ENDNOTE3
    fi
    exit 0
fi

# Specify whether to use COLOR for disassembly output.
# How to visualize jumps in assembly output.
if [ -z "$__OBJDUMP_DISASSEMBLY_COLOR__" ] ; then
    export __OBJDUMP_VISUALIZE_JUMPS__="--no-show-raw-insn --visualize-jumps=color"
    export __OBJDUMP_DISASSEMBLY_COLOR__="--disassembler-color=on"
else
    export __OBJDUMP_VISUALIZE_JUMPS__="--no-show-raw-insn --visualize-jumps"
fi

# See if we can use color.  Some versions of objdump don't allow this (boo!)
objdump "$__OBJDUMP_DISASSEMBLY_COLOR__" "$(which echo)" >/dev/null 2>&1 || {
    export __OBJDUMP_DISASSEMBLY_COLOR__=
}

# Specify the disassembly flavor to use for X86 assembly.
[ -z "$__OBJDUMP_DISASSEMBLY_FLAVOR__" ] && export __OBJDUMP_DISASSEMBLY_FLAVOR__="-Mintel"

# Pick the specific pager to use.  If less is used, it requires a switch to pass
# color codes.  If __PAGER__ is already defined, do not change it.
if [ -z "$__PAGER__" ] ; then
    __PAGER__=$(which bat || which most || which less || which more)
    __PAGER__=${__PAGER__/less/less -r}
    export __PAGER__
fi

[ -z "$__OBJDUMP_OPTS__" ] && export __OBJDUMP_OPTS__=""

# Disassemble in the Intel flavor.
alias dis='objdump -d ${__OBJDUMP_DISASSEMBLY_COLOR__} ${__OBJDUMP_DISASSEMBLY_FLAVOR__} ${__OBJDUMP_OPTS__}'

# Disassemble in the Intel flavor and show jumps.
alias vdis='objdump -d ${__OBJDUMP_DISASSEMBLY_COLOR__} ${__OBJDUMP_DISASSEMBLY_FLAVOR__} ${__OBJDUMP_VISUALIZE_JUMPS__} ${__OBJDUMP_OPTS__}'

# Disassemble in the Intel flavor with pager.
function dism () {
    dis "$@" | ${__PAGER__}
}

# Disassemble in the Intel flavor and show jumps with pager.
function vdism () {
    vdis "$@" | ${__PAGER__}
}
